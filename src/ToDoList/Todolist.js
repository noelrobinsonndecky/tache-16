// Mon composant ToDoList

import React from "react";
import TodoItem from "../ToDoItem/TodoItem";


class Todolist extends React.Component{
    
    render(){

        const { todos } = this.props;

        return (
            <div className="TodolistContainer">
                {
                    todos.map((_todo, _index) => {
                        return (
                            <TodoItem className='maListe' updateTodoFn={this.updateTodo} key={_index} todo={_todo}></TodoItem>
                        )
                    })
                }
            </div>
        )
    }

    updateTodo = (todo) => {
        this.props.updateTodoFn(todo);
    }
}


export default Todolist;