// Mon composant ToDoItem

import React from "react";


class TodoItem extends React.Component{

    render(){


        const { todo } = this.props;

        return (
                <div className={"TodoItem" + (todo.completed ? ' completed' : '')} onClick={this.toogleTodo}>
                    {todo.text}
                </div>
        )
    }

    toogleTodo = () => {
        this.props.updateTodoFn(this.props.todo);
    }
}

export default TodoItem;