// Mon composant AddToDo

import React from 'react';
import '../ToDoItem/TodoItem.css';

class AddTodo extends React.Component{

    constructor() {
        super();
        this.state= {
            todo: ''
        }
    }

    render (){

        return (
            <div className='Addtodo'>
            <form onSubmit={(e) => this.submitTodo(e)} >
                <input id='addToDoInput' onChange={(e) => this.updateInput(e)} type="text" placeholder='Votre tache'/>
                <br/>
                <br/>
                <button type="submit">Ajouter une tache</button>
            </form>
        </div>
        )

    }

    updateInput = (e) => {
        this.setState({ todo: e.target.value});
    }
    submitTodo = (e) => {
        e.preventDefault();
        this.props.addTodoFn(this.state.todo);
        document.getElementById('addToDoInput').value='';
    }
}


export default AddTodo;